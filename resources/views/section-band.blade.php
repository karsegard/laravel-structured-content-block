<section class="section-band--left color--white">
    {!!$image??''!!}
    {!!$headline!!}
    <div class="band--primary  ">
        <!-- <button class="btn btn--primary">boutique en ligne</button> -->
        <button x-data="" x-on:click="window.location.href='/shop'" class="btn btn--primary">boutique en ligne</button>
        <div class="text-area">
            <h4>Une libraire de livres d’occasion «&nbsp;presque neufs&nbsp;»</h4>
            <p class="ejs-paragraph">


                Les livres mis en
                vente sont d’édition récente et en parfait état. Notre choix est
                constamment renouvelé grâce aux clients qui viennent recycler leurs
                livres.
            </p>

        </div>

    </div>


</section>
