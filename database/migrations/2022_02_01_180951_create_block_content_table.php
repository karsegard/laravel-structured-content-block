<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('cm_data_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('description')->nullable();
            $table->text('fields')->nullable();
            $table->text('repeatable')->nullable();
            $table->timestamps();
        });

        Schema::create('cm_bloc_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('template');   
            $table->unsignedBigInteger('data_type_id')->nullable();
        });
       
        Schema::create('cm_slots', function (Blueprint $table) {
            $table->id();
            $table->string('name');  
            $table->unsignedBigInteger('bloc_type_id');          
        });

        Schema::create('cm_bloc_slot_accepts', function (Blueprint $table) {
            $table->unsignedBigInteger('slot_id');
            $table->unsignedBigInteger('bloc_type_id');
        });
       

       /* Schema::create('cm_bloc_type_slots', function (Blueprint $table) {
            $table->unsignedBigInteger('bloc_type_id');
            $table->unsignedBigInteger('slot_id');
        });
*/
        /*

        Schema::create('cm_content_types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->foreignId('data_type_id')->nullable()->constrained('cm_data_types')->onDelete('restrict');
            $table->string('hint')->nullable();
            $table->timestamps();
        });

        Schema::create('cm_bloc_contents', function (Blueprint $table) {
            $table->id();
            $table->string('title');
            $table->unsignedBigInteger('bloc_type_id')->nullable();
            $table->timestamps();
        });

        Schema::create('cm_bloc_content_contents', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('slot_id');
            $table->text('value');
            $table->unsignedBigInteger('bloc_type_id');
        });*/

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cm_bloc_contents');
        Schema::dropIfExists('cm_bloc_content_contents');
        Schema::dropIfExists('cm_bloc_types');
        Schema::dropIfExists('cm_bloc_type_slots');
        Schema::dropIfExists('cm_bloc_slot_accepts');
        Schema::dropIfExists('cm_content_types');
        Schema::dropIfExists('cm_data_types');
    }
}
