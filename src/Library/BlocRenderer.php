<?php

namespace KDA\BCM\Library;

use Illuminate\View\View;

class BlocRenderer
{

    public function __construct()
    {


        $bcm = config('kda.bcm');
        $this->definition = new Collections\Definition($bcm);
    }


    public function render($content_name,$ns="kda-bcm::")
    {
        $views = "";
        foreach ($this->definition->content[$content_name]->content as $content) {
            $views.= $this->render_block($content,$ns)->render();
        }
        return $views;
    }

    public function render_block($block,$ns="kda-bcm::")
    {

        $template = $block->template();
        dump($block,$template,$block->values,$block->slots);

        $values = $block->values ?? [];
        $slots = [];
        foreach ($block->slots as $slot) {
            
            $slots[$slot->original_key]= $this->render_block($slot,$ns);
        }

        if ($template) {
            return view($ns.$template,array_merge($values,$slots));

        }
        else {
            throw new \Error('missing template for'.$content_name);
        }
    }
}
