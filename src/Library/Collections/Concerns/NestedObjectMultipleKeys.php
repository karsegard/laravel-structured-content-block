<?php
namespace KDA\BCM\Library\Collections\Concerns;


interface NestedObjectMultipleKeys{

    public static function getAccessorsKeys():array;
    public static function getClassByKey($key);
}
