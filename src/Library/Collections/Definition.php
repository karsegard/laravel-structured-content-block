<?php

namespace KDA\BCM\Library\Collections;


class SlotAccept extends NestedObject
{


    public function block()
    {
        return  $this->root->blocks->where('original_key', $this->data)->first();
    }
}

class SlotDefinition extends NestedObject
{

    public static function getAccessorsKeys(): array
    {
        return ['accepts'];
    }
    public static function getClassByKey($key)
    {
        switch ($key) {
            case 'accepts':
                return SlotAccept::class;
                break;
        }
    }

    public function accepts()
    {
        return $this->accepts->map(function ($item) {
            return $item->block();
        });
    }
}

class BlockDefinition extends NestedObject
{
    use Traits\AccessorsAreAssociative;

    public static function getAccessorsKeys(): array
    {
        return ['slots'];
    }
    public static function getClassByKey($key)
    {
        switch ($key) {
            case 'slots':
                return SlotDefinition::class;
                break;
        }
    }
    public function accepts()
    {
        return $this->slots->map(function ($item) {
            return $item->accepts();
        });
    }
}

class TypeDefinition  extends NestedObject
{
}

class ContentDefinition  extends NestedObject implements Concerns\NestedObjectMultipleKeys
{

    public function block_type()
    {
        if ($this->block_type) {
            return  $this->root->blocks
                ->where('original_key', $this->block_type)
                ->first();
        }
    }

    public function has_content()
    {
        return $this->content->count() > 0;
    }


    public static function getAccessorsKeys(): array
    {
        return ['content', 'slots'];
    }
    public static function getClassByKey($key)
    {
        switch ($key) {
            case 'content':
                return ContentDefinition::class;
                break;
            case 'slots':
                return ContentDefinition::class;
                break;
        }
    }

    public function template()
    {

        //dump($this);
        return $this->template ?? $this->block_type()->template ?? NULL;
    }

    public function getContent(){
        
    }
}


class Definition extends NestedObject implements Concerns\NestedObjectMultipleKeys
{
    use Traits\AccessorsAreAssociative;

    public static function getAccessorsKeys(): array
    {
        return ['blocks', 'types', 'content'];
    }


    public static function getClassByKey($key)
    {
        switch ($key) {
            case 'blocks':
                return BlockDefinition::class;
                break;
            case 'types':
                return TypeDefinition::class;
                break;
            case 'content':
                return ContentDefinition::class;
                break;
        }
    }
}
