<?php
namespace KDA\BCM\Library\Collections;


class NestedArray {

    public function __construct($data){
        if(is_callable(['static','getAccessorKey'])){
            $key = static::getAccessorKey();
            $this->$key = collect($data)->map( function ($item){
                $class =static::getClass();
                $o =  new $class($item);
                return $o;
            });

        }else{
            $this->data= $data;
        }

    }


    public function __get($key){
        return $this->data[$key];
    }

}