<?php

namespace KDA\BCM;


use KDA\Laravel\PackageServiceProvider;
use Illuminate\Support\Facades\Blade;
use PragmaRX\Yaml\Package\Facade as YAML;
class ServiceProvider extends PackageServiceProvider
{
    use \KDA\Laravel\Traits\HasYamlConfig;
    use \KDA\Laravel\Traits\HasViews;
   
    protected $configs = [
        'kda/bcm.yaml'=> 'kda.bcm'
    ];
    protected $viewNamespace = 'kda-bcm';

    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }


    protected function bootSelf(){
     


    }


   

}
